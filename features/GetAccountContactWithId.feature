Feature: Get Account Contact with id
  Gets the account contact with the provided id

  Scenario : Success
    Given I provide the accountContactId of an existing accountContact in the account-contact-service
    And provide an accessToken identifying me as a partner rep associated with accountContactId
    When I execute getAccountContactWithId
    Then I should receive the account contact
