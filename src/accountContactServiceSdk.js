import AccountContactServiceSdkConfig from './accountContactServiceSdkConfig';
import DiContainer from './diContainer';
import AddAccountContactReq from './addAccountContactReq';
import AddAccountContactFeature from './addAccountContactFeature';
import AccountContactSynopsisView from './accountContactSynopsisView';
import AccountContactView from './accountContactView';
import GetAccountContactWithIdFeature from './getAccountContactWithIdFeature';
import ListAccountContactsWithAccountIdFeature from './listAccountContactsWithAccountIdFeature';
import AccountContactId from './accountContactId';

/**
 * @class {AccountContactServiceSdk}
 */
export default class AccountContactServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {AccountContactServiceSdkConfig} config
     */
    constructor(config:AccountContactServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    addAccountContact(request:AddAccountContactReq,
                      accessToken:string):Promise<AccountContactId> {

        return this
            ._diContainer
            .get(AddAccountContactFeature)
            .execute(
                request,
                accessToken
            );

    }

    getAccountContactWithId(id:string,
                            accessToken:string):Promise<AccountContactView> {

        return this
            ._diContainer
            .get(GetAccountContactWithIdFeature)
            .execute(
                id,
                accessToken
            );
    }

    listAccountContactsWithAccountId(accountId:string,
                                     accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListAccountContactsWithAccountIdFeature)
            .execute(
                accountId,
                accessToken
            );

    }

}
