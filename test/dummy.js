/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default {
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    iso31661Alpha2Code: 'US',
    iso31662Code:'WA',
    url: 'https://test-url.com',
    sapAccountNumber: '0000000000',
    sapVendorNumber: '0000000000',
    userId: '0000000000',
    //accountId:'000000000000000000',
    accountId:'001A000001DcxdmIAB',
    //accountContactId:'000000000000000000',
    accountContactId:'0036C000003HiMdQAK',
    emailAddress:'test@test.com'
};
