import AddAccountContactReq from '../../src/addAccountContactReq';
import dummy from '../dummy';

/*
 test methods
 */
describe('AddAccountContactReq class', () => {
    describe('constructor', () => {
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        null,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress,
                        dummy.iso31661Alpha2Code,
                        dummy.iso31662Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');
        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    expectedAccountId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress,
                    dummy.iso31661Alpha2Code,
                    dummy.iso31662Code
                );

            /*
             assert
             */
            const actualAccountId = objectUnderTest.accountId;
            expect(actualAccountId).toEqual(expectedAccountId);
        });
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        dummy.accountId,
                        null,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress,
                        dummy.iso31661Alpha2Code,
                        dummy.iso31662Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');
        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    expectedFirstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress,
                    dummy.iso31661Alpha2Code,
                    dummy.iso31662Code
                );

            /*
             assert
             */
            const actualFirstName = objectUnderTest.firstName;
            expect(actualFirstName).toEqual(expectedFirstName);
        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        dummy.accountId,
                        dummy.firstName,
                        null,
                        dummy.phoneNumber,
                        dummy.emailAddress,
                        dummy.iso31661Alpha2Code,
                        dummy.iso31662Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');
        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    dummy.firstName,
                    expectedLastName,
                    dummy.phoneNumber,
                    dummy.emailAddress,
                    dummy.iso31661Alpha2Code,
                    dummy.iso31662Code
                );

            /*
             assert
             */
            const actualLastName = objectUnderTest.lastName;
            expect(actualLastName).toEqual(expectedLastName);
        });
        it('throws if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        dummy.accountId,
                        dummy.firstName,
                        dummy.lastName,
                        null,
                        dummy.emailAddress,
                        dummy.iso31661Alpha2Code,
                        dummy.iso31662Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'phoneNumber required');
        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.phoneNumber;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    dummy.firstName,
                    dummy.lastName,
                    expectedPhoneNumber,
                    dummy.emailAddress,
                    dummy.iso31661Alpha2Code,
                    dummy.iso31662Code
                );

            /*
             assert
             */
            const actualPhoneNumber = objectUnderTest.phoneNumber;
            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);
        });
        it('throws if emailAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        dummy.accountId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        null,
                        dummy.iso31661Alpha2Code,
                        dummy.iso31662Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'emailAddress required');
        });
        it('sets emailAddress', () => {
            /*
             arrange
             */
            const expectedUserId = dummy.emailAddress;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    expectedUserId,
                    dummy.iso31661Alpha2Code,
                    dummy.iso31662Code
                );

            /*
             assert
             */
            const actualUserId = objectUnderTest.emailAddress;
            expect(actualUserId).toEqual(expectedUserId);
        });
        it('throws if countryIso31661Alpha2Code is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        dummy.accountId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress,
                        null,
                        dummy.iso31662Code
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'countryIso31661Alpha2Code required');
        });
        it('sets countryIso31661Alpha2Code', () => {
            /*
             arrange
             */
            const expectedCountryIso31661Alpha2Code = dummy.iso31661Alpha2Code;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress,
                    expectedCountryIso31661Alpha2Code,
                    dummy.iso31662Code
                );

            /*
             assert
             */
            const actualCountryIso31661Alpha2Code = objectUnderTest.countryIso31661Alpha2Code;
            expect(actualCountryIso31661Alpha2Code).toEqual(expectedCountryIso31661Alpha2Code);
        });
        it('throws if regionIso31662Code is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new AddAccountContactReq(
                        dummy.accountId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.phoneNumber,
                        dummy.emailAddress,
                        dummy.iso31661Alpha2Code,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'regionIso31662Code required');
        });
        it('sets regionIso31662Code', () => {
            /*
             arrange
             */
            const expectedRegionIso31662Code = dummy.iso31662Code;

            /*
             act
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress,
                    dummy.iso31661Alpha2Code,
                    expectedRegionIso31662Code
                );

            /*
             assert
             */
            const actualRegionIso31662Code = objectUnderTest.regionIso31662Code;
            expect(actualRegionIso31662Code).toEqual(expectedRegionIso31662Code);
        });
    });
    describe('toJSON method', () => {
        it('returns expected object', () => {
            /*
             arrange
             */
            const objectUnderTest =
                new AddAccountContactReq(
                    dummy.accountId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.phoneNumber,
                    dummy.emailAddress,
                    dummy.iso31661Alpha2Code,
                    dummy.iso31662Code
                );

            const expectedObject =
            {
                accountId: objectUnderTest.accountId,
                firstName: objectUnderTest.firstName,
                lastName: objectUnderTest.lastName,
                phoneNumber: objectUnderTest.phoneNumber,
                emailAddress: objectUnderTest.emailAddress,
                countryIso31661Alpha2Code: objectUnderTest.countryIso31661Alpha2Code,
                regionIso31662Code:objectUnderTest.regionIso31662Code
            };

            /*
             act
             */
            const actualObject =
                objectUnderTest.toJSON();

            /*
             assert
             */
            expect(actualObject).toEqual(expectedObject);

        })
    });
});
